import React from "react";
import { Button, TextField } from "@material-ui/core";
const Search = props => {
  return (
    <form noValidate autoComplete="on" style={{ float: "right" }}>
      <TextField id={props.id} label={props.label} />
      <Button color="primary">Search {props.label}</Button>
    </form>
  );
};

export default Search;
