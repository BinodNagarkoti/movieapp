import React, { Component } from "react";
import TopNavBar from "./TopNavBar";

class Home extends Component {
  state = {};
  render() {
    return (
      <div className="main">
        <TopNavBar nosearch={true} />
        <div className="home-welcomText">
          <p>
            Welcome to our the <b>"Movie And Tv Shows App"</b>, a place to
            search your long awaited movie and to know more about it. This web
            site is built using React JS library and along with react Bootstrap
            which is CSS framework. The movies and TvShows data are taken from
            the TMDB (the movie DataBase) API
          </p>
        </div>
      </div>
    );
  }
}

export default Home;
