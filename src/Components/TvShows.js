import React, { Component } from "react";
import { trendingTvShowsWeek, searchTvShows } from "../assets/config/config";
import { genres } from "../assets/config/genres";
import TopNavBar from "./TopNavBar";
import deaultImg from "../assets/images/nothumb.png";
import rating from "../assets/icons/star-circle.png";
import { OverlayTrigger, Popover } from "react-bootstrap";

class TvShows extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tvShows: [],
      search: false,
      selectedMovie: [],
      selectedTvShow: [],
      select: false,
      total_results: 0,
      keyWords: "",
      zeroResultsError: "",
      searchResultSuccess: "",
      searchResultMsg: ""
    };
  }

  onSearch = event => {
    const search = event.target.value;
    // console.log("search", search);
    fetch(`${searchTvShows}${search}`)
      .then(response => response.json())
      .then(data => {
        if (data.total_results !== 0 && typeof data.results !== "undefined") {
          return this.setState(
            prevState => {
              return {
                tvShows: data.results,
                total_results: data.total_results,
                search: true,
                searchResultMsg: `We Found
              ${data.total_results} 
               Tv Shows with '${search}' Keywords`
              };
            },
            () => console.log("Searched tvShows List:", data.results)
          );
        } else {
          this.state.setState({
            searchResultMsg: `Sorry, No TvShows Found with '${search}' Keywords `
          });
        }
      })
      .catch(err => {
        console.log("error", err);
        this.setState({ movies: {} });
      });
  };
  componentDidMount() {
    // console.log("length", this.state.tvShows.length);
    fetch(trendingTvShowsWeek)
      .then(response => response.json())
      .then(data =>
        this.setState({ tvShows: data.results }, () =>
          console.log("tv shows", this.state.tvShows)
        )
      )
      .catch(err => {
        console.log("error", err);
        this.setState({ tvShows: {} });
      });
  }
  render() {
    let tvShows = [];
    if (this.state.tvShows) {
      this.state.tvShows.map((tvShow, index) => {
        return tvShows.push(
          <OverlayTrigger
            key={index}
            trigger="hover"
            placement="auto"
            overlay={
              <Popover id={`popover-positioned-top`}>
                <Popover.Title as="h3">{tvShow.original_name}</Popover.Title>
                <Popover.Content>
                  <h5>Overview:</h5>
                  {tvShow.overview}
                </Popover.Content>
              </Popover>
            }
          >
            <div className="movies">
              <div className="box img">
                <img
                  style={{ width: "210px", height: "313px" }}
                  src={
                    tvShow.poster_path === null
                      ? tvShow.backdrop_path === null
                        ? deaultImg
                        : `https://image.tmdb.org/t/p/w200/${tvShow.backdrop_path}`
                      : `https://image.tmdb.org/t/p/w200/${tvShow.poster_path}`
                  }
                  alt="123"
                />
                <h6>{tvShow.original_name}</h6>
                <span>
                  {tvShow.first_air_date && tvShow.first_air_date.split("-")[0]}
                </span>
              </div>

              <div className="details box stack-top">
                <img
                  className="star"
                  style={{ marginTop: "47px", marginBottom: "7px" }}
                  src={rating}
                  alt="rating"
                />
                <h4 className="rt">{tvShow.vote_average}/10</h4>
                <div className="gen">
                  {tvShow.genre_ids.splice(0, 2).map((genre, index) => {
                    const found = genres.find(el => el.id === genre);

                    if (typeof found === "undefined") {
                      return "";
                    }

                    return <h5 key={found.id}> {found.name}</h5>;
                  })}
                </div>
              </div>
            </div>
          </OverlayTrigger>
        );
      });
    }

    return (
      <div className="main">
        <TopNavBar onChange={this.onSearch.bind(this)} />
        {this.state.search ? (
          <h5>The Search results are: </h5>
        ) : (
          <div>
            <img src={rating} alt="rating" /> <h5>Today's Trending Tv Shows</h5>
          </div>
        )}
        <p>{this.state.searchResultMsg}</p>
        <div className="wrapper">{tvShows}</div>
      </div>
    );
  }
}

export default TvShows;
