import React, { Component } from "react";

import { Navbar, Nav, Form, FormControl } from "react-bootstrap/";
// import { BrowserRouter as Router, Route, Link, Switch } from "react-router-dom";

class TopNavBar extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <Navbar collapseOnSelect expand="sm" bg="dark" variant="dark">
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="mr-auto">
            {/* <Nav.Link href="/">Error Page</Nav.Link>  for devlopement only*/}

            <Nav.Link href="/">Home</Nav.Link>
            <Nav.Link href="/tvshows">TV Shows</Nav.Link>
            <Nav.Link href="/movies">Movies</Nav.Link>
            {/* <Nav.Link href="/tmovies">Checking</Nav.Link> for development only */}
          </Nav>
        </Navbar.Collapse>
        {this.props.nosearch ? (
          ""
        ) : (
          <Form>
            <FormControl
              type="text"
              placeholder="Search"
              onChange={this.props.onChange}
            />
          </Form>
        )}
      </Navbar>
    );
  }
}

export default TopNavBar;
