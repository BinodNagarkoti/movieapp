import React, { Component } from "react";
import { trandingMovie, api, searchMovie } from "../assets/config/config";
import rating from "../assets/icons/star-circle.png";
import { genres } from "../assets/config/genres";
import TopNavBar from "./TopNavBar";
import deaultImg from "../assets/images/nothumb.png";
import { Button } from "react-bootstrap/";
// import Search from "../Components/search";
class Movies extends Component {
  constructor(props) {
    super(props);
    this.state = {
      movies: [],
      search: false,
      selectedMovie: [],
      selectedTvShow: [],
      select: false,
      searchText: "",
      videosDetails: {},
      modelShow: false
    };
  }
  onSearch = event => {
    const search = event.target.value;
    console.log("search", search);
    fetch(`${searchMovie}${search}&api_key=${api}`)
      .then(response => response.json())
      .then(data =>
        this.setState(
          {
            movies: data.results,
            total_results: data.total_results,
            search: true,
            searchResultMsg: `We Found
        ${data.total_results} 
         Movies with '${search}' Keywords`
          },
          () => console.log("movielist:", this.state.movies)
        )
      )
      .catch(err => {
        console.log("error", err);
        this.setState({ movies: {} });
      });
  };

  genre = genre_ids => {
    genre_ids.splice(0, 3).map((genre, index) => {
      const found = genres.find(el => el.id === genre);

      if (typeof found === "undefined") {
        return "";
      }

      return found.name;
    });
    return genres;
  };

  componentDidMount() {
    console.log("length", this.state.movies.length);
    fetch(trandingMovie)
      .then(response => response.json())
      .then(data =>
        this.setState({ movies: data.results }, () =>
          console.log("movielist:", this.state.movies)
        )
      )
      .catch(err => {
        console.log("error", err);
        this.setState({ movies: {} });
      });
  }
  render() {
    console.log("location", this.props.location);
    console.log(genres.genres);
    let movies = [];
    // console.log("movie length", this.state.movies.length);
    if (this.state.movies) {
      this.state.movies.map((movie, index) => {
        return movies.push(
          <div className="movies" key={index}>
            <div className="box img">
              <img
                style={{ width: "210px", height: "315px" }}
                srcSet={
                  movie.poster_path === null
                    ? movie.backdrop_path === null
                      ? deaultImg
                      : `https://image.tmdb.org/t/p/w200/${movie.backdrop_path}`
                    : `https://image.tmdb.org/t/p/w200/${movie.poster_path}`
                }
                alt="123"
              />
              <div className="figcaption">
                <h6>
                  {movie.original_title && movie.original_title.split("(")[0]}
                </h6>

                <span> {movie.release_date && movie.release_date.split("-")[0]}</span>
              </div>
            </div>
            <div className="details hvr-radial-out box stack-top">
              <img
                style={{ marginTop: "47px", marginBottom: "7px" }}
                src={rating}
                alt="rating"
              />
              <h4 className="rt">{movie.vote_average}/10</h4>
              <div className="gen">
                {movie.genre_ids.splice(0, 2).map((genre, index) => {
                  const found = genres.find(el => el.id === genre);

                  if (typeof found === "undefined") {
                    return <div></div>;
                  }

                  return <h4 key={index}> {found.name}</h4>;
                })}

                <a href={`/movies/${movie.id}`}>
                  <Button variant="primary">View Details</Button>
                </a>
              </div>
            </div>
          </div>
        );
      });
    }
    console.log("searching", this.state.searchText);
    return (
      <div className="main">
        {/* Nav.Link  or Link. in Link Component we use "to" attributes instead of "href". */}
        <TopNavBar onChange={this.onSearch.bind(this)} />
        {this.state.search ? (
          <h5 className="searchMsg">The Search results are: </h5>
        ) : (
          <div className="trending">
            <img src={rating} alt="rating" /> <h5>Today's Trending Movies</h5>
          </div>
        )}
        <p> {this.state.searchResultMsg}</p>

        <div className="wrapper">{movies}</div>
      </div>
    );
  }
}

export default Movies;
