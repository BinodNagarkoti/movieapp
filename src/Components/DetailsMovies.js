import React, { Component } from "react";
import {
  api,
  searchMovieVideos,
  STR_VIDEOS,
  moviedetails,
  similarMovies,
} from "../assets/config/config";
// import rating from "../assets/icons/star-circle.png";
import { genres } from "../assets/config/genres";
import TopNavBar from "./TopNavBar";
import deaultImg from "../assets/images/nothumb.png";
import { Button } from "react-bootstrap/";

class MovieDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: this.props.match.params.id,
      path: this.props.match.path.split("/")[1],
      youtube: [],
    };
    this.getDetails = this.getDetails.bind(this);
    console.log("from constructor", this.state);
    // this.getDetails(this.state.id);
  }
  componentDidMount() {
    this.getDetails(this.state.id);
    fetch(
      `${similarMovies}${this.state.id}/similar?api_key=${api}&language=en-US&page=1`
    )
      .then((response) => response.json())
      .then((data) =>
        this.setState(
          { movies: data.results },
          console.log("Similar movies", this.state.movies)
        )
      )

      .catch((err) => {
        console.log("error", err);
        this.setState({ message: "no data found" });
      });
  }
  getDetails = (id) => {
    let youtube = "";
    fetch(`${searchMovieVideos}${id}/${STR_VIDEOS}?api_key=${api}`)
      .then((response) => response.json())
      .then((data) => {
        youtube = data.results.splice(0, 3);
        console.log("video results", youtube);
        return youtube;
      })

      .catch((err) => {
        console.log("error", err);
        this.setState({ message: "no data found" });
      });
    fetch(`${moviedetails}${id}?api_key=${api}`)
      .then((response) => response.json())
      .then((data) => {
        //   console.log("movie details-->:", data);
        const {
          title,
          tagline,
          vote_average,
          status,
          runtime,
          release_date,
          original_language,
          genres,
          overview,
          poster_path,
          backdrop_path,
        } = data;
        this.setState(
          {
            title,
            tagline,
            vote_average,
            status,
            runtime,
            release_date,
            overview,
            original_language,
            genres,
            poster_path,
            backdrop_path,
            youtube,
          },

          () => console.log("videosDetails:", this.state.youtube)
        );
      })
      .catch((err) => {
        console.log("error", err);
        this.setState({ message: "no data found" });
      });
  };

  render() {
    return (
      <div className="holder">
        <TopNavBar nosearch={true} />
        <div className="breadcrumbs">
          Movies / Movies Details- ({this.state.title}) <br />
        </div>
        <div className="holder_body">
          <div className="poster">
            <img
              srcSet={
                this.state.poster_path === null
                  ? deaultImg
                  : `https://image.tmdb.org/t/p/w300/${this.state.poster_path}`
              }
              alt="123"
            />
          </div>
          <div className="details">
            <h1>{this.state.title}</h1>
            <div className="tagline">
              {this.state.tagline === "" ? (
                ""
              ) : (
                <span>"{this.state.tagline}"</span>
              )}
            </div>
            <h3>Overview</h3>
            <p>{this.state.overview}</p>
            <div className="others">
              {" "}
              <h6>Date: {this.state.release_date}</h6>
              <h6>
                Genres:
                {genres.splice(0, 3).map((item, index) => (
                  <span key={index}> {item.name} </span>
                ))}{" "}
              </h6>
              <h6> Rating: {this.state.vote_average} </h6>
              <h6> Status: {this.state.status}</h6>
              <h6> Duration: {this.state.runtime} Minutes</h6>
              <h6> Language: {this.state.original_language}</h6>
            </div>

            <a href="/movies">
              <Button>Go Back</Button>
            </a>
          </div>
        </div>
        <div className="trailer">
          {/* {this.state.youtube[0].key} */}
          {this.state.youtube &&
            this.state.youtube.map((item, index) => (
              <iframe
                key={index}
                title={index}
                width="375"
                height="200"
                src={`https://www.youtube.com/embed/${item.key}`}
                frameBorder="0"
                allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                allowFullScreen
              />
            ))}
        </div>
        <div className="holder_footer"></div>
      </div>
    );
  }
}
export default MovieDetails;
