export const api = "7d482b0654c3c29aefa2a224082cee00";

const baseURl = `https://api.themoviedb.org/3`;
export const STR_VIDEOS = "videos";
export const youtubeVideo = "https://www.youtube.com/watch?v=";
export const trandingMovie = `${baseURl}/trending/movie/day?api_key=${api}`;
export const searchMovie = `${baseURl}/search/movie?query=`;
export const searchMovieVideos = `${baseURl}/movie/`;
export const searchTvShows = `${baseURl}/search/tv?api_key=${api}&query=`;
export const similarMovies = `https://api.themoviedb.org/3/movie/`
export const trendingTvShowsWeek = `${baseURl}/trending/tv/week?api_key=${api}`;
export const WelcomeText = `Welcome to the <b>Movie App</b>, a place to search your long awaited
movie and to know more about it. This web site lite easy to use due
to minimal user interface`;
export const moviedetails = `${baseURl}/movie/`;
