import React, { Component } from "react";
import "./App.css";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Movies from "./Components/Movies";
import TMovies from "./Components/T-Movies";
import DetailsMovies from "./Components/DetailsMovies";
import TvShows from "./Components/TvShows";
import Home from "./Components/Home";
import { Button } from "react-bootstrap/";

// import TopNavBar from "./Components/TopNavBar";

class App extends Component {
  constructor() {
    super();
    this.state = {};
  }

  render() {
    return (
      <div>
        <Router>
          <Switch>
            <Route exact path="/" component={Home} />
            <Route exact path="/tvshows" component={TvShows} />

            <Route exact path="/movies" component={Movies} />
            <Route exact path="/movies/:id" component={DetailsMovies} />
            {/* <Route exact path="/tvmovies/:id" component={DetailsMovies} /> */}
            <Route exact path="/tmovies" component={TMovies} />
            <Route
              render={() => (
                <h1 className="error">
                  404 Error Page not Found <br />
                  <span role="img" aria-label="emoji">&#128552;</span>
                  <a href="/">
                    <Button>Go To Home Page</Button>
                  </a>
                </h1>
              )}
            />
          </Switch>
        </Router>
      </div>
    );
  }
}

export default App;
